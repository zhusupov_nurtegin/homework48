﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Task
{
   public  class MyNode : IDisposable
    {
        private bool disposed;
        public object _Data;
   
        protected MyNode _Next;
        protected MyNode _Prev;
        private object newElement;

        public MyNode()
        {
         
        }

        public MyNode(object worse)
        {
            this.newElement = worse;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Console.WriteLine("disposed");
                }
           
            }
            disposed = true;
     
        }

        void DoNodes()
        {
            using (var Linked = new Linked_Lists())
            {
                Console.WriteLine(Linked.names);
            }
          
        }



        public MyNode Next
        {
            get { return this._Next; }
            set { this._Next = value; }
        }
        public MyNode Prev
        {
            get { return this._Prev; }
            set { this._Prev = value; }
        }
        public virtual void PrintSomething()
        {
            Console.WriteLine("Alex");
            Console.WriteLine("Tina");
            Console.WriteLine("Max");
            Console.WriteLine("Juli");

        }
    }


    public class Linked_Lists : IDisposable
    {

        private MyNode First;
        private MyNode Current;
        private MyNode Last;
        private uint size;
        public List<string> names;

       

        public Linked_Lists() 
        {

            size = 421;
            First = Current = Last = null;
            this.names = new List<string>();
        }

        Lazy<MyNode> Node = new Lazy<MyNode>();
        public void Dispose()
        {
            if (this.names != null)
            {
                this.names.Clear();
              
                this.names = null;
                Node.Value.Dispose();
                Console.WriteLine("disposed");
            }
            using (var Nodes = new MyNode())
            {
                Console.WriteLine(Nodes._Data);
            }

        }
     
   
        public bool isEmpty
        {
            get
            {
                return size == 0;
            }
        }

        public void Add(object new_Data)
        {
            MyNode node = new MyNode(new_Data);
          
            if (First == null)
            {
                First = node;
                Last = node;
                Last.Next = First;
            }
            else
            {
                node.Next = First;
                Last.Next = node;
                Last = node;
            }
        }

      
     

        public void Insert_Index(object newElement, uint index)
        {
            if (index < 1 || index > size)
            {
                throw new InvalidOperationException();
            }
            else if (index == 1)
            {
                Push_Front(newElement);
            }
            else if (index == size)
            {
                Push_Back(newElement);
            }
            else
            {
                uint count = 1;
                Current = First;
                while (Current != null && count != index)
                {
                    Current = Current.Next;
                    count++;
                }
                MyNode newNode = new MyNode(newElement);
                Current.Prev.Next = newNode;
                newNode.Prev = Current.Prev;
                Current.Prev = newNode;
                newNode.Next = Current;
            }
        }

        public void Push_Front(object newElement)
        {
            MyNode newNode = new MyNode(newElement);

            if (First == null)
            {
                First = Last = newNode;
            }
            else
            {
                newNode.Next = First;
                First = newNode;
                newNode.Next.Prev = First;
            }
        }

        public void Push_Back(object newElement)
        {
            MyNode newNode = new MyNode(newElement);

            if (First == null)
            {
                First = Last = newNode;
            }
            else
            {
                Last.Next = newNode;
                newNode.Prev = Last;
                Last = newNode;
            }

        }
  
        public MyNode Pop_Back()
        {
            if (Last == null)
            {
                throw new InvalidOperationException();
            }
            else
            {
                MyNode temp = Last;
                if (Last.Prev != null)
                {
                    Last.Prev.Next = null;
                }
                Last = Last.Prev;

                return temp;
    
    }
     
    }
        class Program
        {
            static void Main()
            {
                
                MyNode nodes = new MyNode();
              
                try
                {
                    nodes.PrintSomething();
                 
                }
                finally
                {
                    nodes.Dispose();
                }

                LinkedList<string> liqs = new LinkedList<string>(new List<string> { "Имена, Фамилии, Отчеты" });
                LinkedList<string> liqs2 = new LinkedList<string>(new List<string> { "Ученики, Проблемы, Учителя" });
                
                try
                {
                    Console.WriteLine(liqs);
                }
                finally { 
                Console.WriteLine(liqs2);
                    }
                nodes.PrintSomething();




            }
        }
    }
}

