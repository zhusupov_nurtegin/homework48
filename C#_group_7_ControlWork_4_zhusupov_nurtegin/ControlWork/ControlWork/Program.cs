﻿using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace ControlWork
{
    public class Task
    {
        public int _idOfTask;
        public string _NameOfTask;
        public string _StatusOfTask;
      
        public DateTime _EnterDateOfTask;
        public DateTime _DeadLine;

        public Task(int id, string Name, string Status,  DateTime date, DateTime Deaddate)
        {
            this._idOfTask = id;
            this._NameOfTask = Name;
            this._StatusOfTask = Status;
         
            this._EnterDateOfTask = date;
            this._DeadLine = Deaddate;
        }
        public Task()
        {

        }


        public static void Sort()
        {
            Sort();
        }

 

        public void PrintTask()
        {

            Console.WriteLine("--------------");
            Console.WriteLine($"ID задачи : {_idOfTask}");
            Console.WriteLine($"Название задачи : {_NameOfTask}");
            Console.WriteLine($"Статус задачи : {_StatusOfTask}");
          
            Console.WriteLine($"Дата создания задачи : {_EnterDateOfTask}");
            Console.WriteLine($"DeadLine Дата задачи: {_DeadLine}");
        }
        public static Task New()
        {

            Random rnd = new Random();

            int value = rnd.Next(44, 99);
            Console.Write("Ваш ID : ");
            Console.WriteLine(value);
            Console.Write("Введите название задачи: ");
            string Name = Console.ReadLine();
            Console.Write("Статус автоматически поставлен на Только введен! Нажмите на Enter, чтобы продолжить.");
            string Status = "Только введен";
            Console.ReadLine();

            
            
           Random gen = new Random();
        DateTime Rad()
        {
            DateTime start = new DateTime(2018, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(gen.Next(range));
        }
            Console.Write("Дата создания задачи :");
            Console.WriteLine(Rad());
            Again:
            Console.Write("Введите временной Дедлайн для вашей задачи : ");
            DateTime Dead = DateTime.Parse(Console.ReadLine());
            if (Dead > Rad())
            {
                Console.WriteLine("Дедлайн не может быть раньше объявления начала задачи. Попробуйте еще раз\n");
                goto Again;
            }
            return new Task(value, Name, Status,  Rad(), Dead);
        }
        public class DataLoader

        {

            const string directory = "../../Data";

            const string fileName = "jsontasks.json";


            public static void Save(Task[] companies)

            {

                string json = JsonConvert.SerializeObject(companies);

                SaveFile(json);

            }


            public static Task[] Load()

            {

                try

                {

                    string content = File.ReadAllText($"{directory}/{fileName}");

                    if (string.IsNullOrEmpty(content))

                    {

                        throw new ApplicationException($"файл {fileName} пустой");

                    }

                    Task[] tasks = JsonConvert.DeserializeObject<Task[]>(content);


                    return tasks;

                }

                catch (DirectoryNotFoundException)

                {

                    Console.WriteLine($"Отсутствует директория {directory}");

                    return new Task[0];

                }

                catch (FileNotFoundException)

                {

                    Console.WriteLine($"Отсутствует файл {fileName}");

                    return new Task[0];

                }

                catch (ApplicationException ex)

                {

                    Console.WriteLine(ex.Message);

                    return new Task[0];

                }

            }


            private static void SaveFile(string content)

            {

                try

                {

                    File.WriteAllText($"{directory}/{fileName}", content);

                }

                catch (DirectoryNotFoundException)

                {

                    Directory.CreateDirectory(directory);

                    SaveFile(content);

                }

            }

        }


        class Program
        {
            delegate void DatePros();
            static void Main(string[] args)
            {
                Console.WriteLine("Здесь представлены примеры отображения ваших задач\n");
                Console.WriteLine("      |");
                Console.WriteLine("      |");
                Console.WriteLine("      V\n");
                    



                DateTime date1 = new DateTime(2015, 7, 20);
                DateTime date2 = new DateTime(2017, 4, 28);
                DateTime date3 = new DateTime(2011, 1, 12);
                DateTime date4 = new DateTime(2013, 2, 26);
                DateTime dead1 = new DateTime(2016, 4, 28);
                DateTime dead2 = new DateTime(2018, 2, 25);
                DateTime dead3 = new DateTime(2012, 3, 11);
                DateTime dead4 = new DateTime(2013, 4, 14);

                List<Task> tasks = new List<Task>()             {


            new Task(){_idOfTask= 11,
                _NameOfTask = "Решить пример из учебника алгербы",
                _StatusOfTask = "Только введен",
           
                _EnterDateOfTask = date1 ,
                _DeadLine = dead1 },

                    new Task(){_idOfTask= 22,
                        _NameOfTask = "Начертить ровный треугольник без линейки",
                        _StatusOfTask = "В прогрессе",
                      
                        _EnterDateOfTask = date2 ,
                        _DeadLine = dead2},

                    new Task(){_idOfTask= 33,
                        _NameOfTask = "Повесить картину на стене",
                        _StatusOfTask = "Сделано",
                      
                        _EnterDateOfTask = date3,
                        _DeadLine = dead3 },

                    new Task(){_idOfTask= 44,
                        _NameOfTask = "Написать руководству компании",
                        _StatusOfTask = "В прогрессе",
                       
                        _EnterDateOfTask = date4 ,
                        _DeadLine = dead4},
            };

                int prior = 1;
                foreach (Task task in tasks.Where(cr => cr._idOfTask >= prior))
                    task.PrintTask();





                int choice;
                string again;
                do
                {
                    Console.WriteLine();
                    Console.WriteLine("1.Добавление новой задачи");
                    Console.WriteLine("2.Выбрать задачу из списка");
                    Console.WriteLine("3.Настройки");
                    Console.WriteLine("4.Сортировка списка задач");

                    choice = Int32.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:

                            tasks.Add(Task.New());
                            break;
                        case 2:

                            Console.WriteLine("Что вы именно хотите сделать?\n 1 - Удалить , 2 - Изменить");
                            int x;
                            x = Int32.Parse(Console.ReadLine());
                            if (x == 1)
                            {
                            Delete:
                                Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы удалить : ");
                                int del = Convert.ToInt32(Console.ReadLine());
                                var ToRemove = tasks.Find(r => r._idOfTask == del);
                                if (ToRemove._StatusOfTask == "В прогрессе")
                                {
                                    Console.WriteLine("Вы не можете удалить эту задачу. Попробуйте другую задачу.");
                                    goto Delete;
                                }
                                if (ToRemove != null)
                                    tasks.Remove(ToRemove);
                                Console.WriteLine($"{ToRemove._idOfTask} , {ToRemove._NameOfTask} , {ToRemove._StatusOfTask} ,  { ToRemove._EnterDateOfTask}");
                                Console.WriteLine("------------Успешно удалено---------------");
                            }
                            else if (x == 2)
                            {
                                Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы изменить его статус : ");
                                int change = Convert.ToInt32(Console.ReadLine());
                                var taskToChange = tasks.Find(c => c._idOfTask == change);

                                if (taskToChange != null)
                                    tasks.IndexOf(taskToChange);
                                Console.WriteLine($"Нынешний статус задачи : {taskToChange._StatusOfTask}\n Нажмите Enter, чтобы подтвердить изменение статуса.");
                                
                                string Status = Console.ReadLine();

                                if (taskToChange._StatusOfTask == "Только введен")
                                {
                                    Console.WriteLine("Статус переименован на В прогрессе");

                                    taskToChange._StatusOfTask = Status;




                                }
                                else if (taskToChange._StatusOfTask == "В прогрессе")
                                {
                                    Console.WriteLine("Статус поменян на Сделано");

                                    taskToChange._StatusOfTask = Status;
                                }
                                else if (taskToChange._StatusOfTask == "Сделано")
                                {
                                    Console.WriteLine("Статус необязателен");
                                    taskToChange._StatusOfTask = Status;





                                }
                            }

                            break;
                        case 3:
                            Console.WriteLine("Выберите действие:");
                            Console.WriteLine("1.Удаление задачи из списка");
                            Console.WriteLine("2.Изменение статуса на “просрочена”");
                            Console.WriteLine("3.Отображение задачи в списке красным шрифтом");
                            int z = Int32.Parse(Console.ReadLine());
                            if (z == 1)
                            {
                            Delete:
                                Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы удалить : ");
                                int del = Convert.ToInt32(Console.ReadLine());
                                var ToRemove = tasks.Find(r => r._idOfTask == del);
                                if (ToRemove._StatusOfTask == "В прогрессе")
                                {
                                    Console.WriteLine("Вы не можете удалить эту задачу. Попробуйте другую задачу.");
                                    goto Delete;
                                }
                                if (ToRemove != null)
                                    tasks.Remove(ToRemove);
                                Console.WriteLine($"{ToRemove._idOfTask} , {ToRemove._NameOfTask} , {ToRemove._StatusOfTask} ,  { ToRemove._EnterDateOfTask}");
                                Console.WriteLine("------------Успешно удалено---------------");
                            }
                            else if (z == 2)
                            {
                                Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы изменить его статус на Просрочено");
                                int change = Convert.ToInt32(Console.ReadLine());
                                var taskToChange = tasks.Find(c => c._idOfTask == change);
                                Console.WriteLine($"Нынешний статус задачи : {taskToChange._StatusOfTask}\n Нажмите Enter, чтобы подтвердить изменение статуса.");

                                string Status = Console.ReadLine();

                                if (taskToChange._StatusOfTask == "Только введен")
                                {
                                     Console.WriteLine("Статус изменен на Просрочен");

                                    taskToChange._StatusOfTask = Status;
                                    Status = "Просрочен";



                                }
                                else if (taskToChange._StatusOfTask == "В прогрессе")
                                {
                                    Console.WriteLine("Статус изменен на Просрочен");

                                    taskToChange._StatusOfTask = Status;
                                    Status = "Просрочен";
                                }
                                else if (taskToChange._StatusOfTask == "Сделано")
                                {
                                    Console.WriteLine("Статус изменен на Просрочен");
                                    taskToChange._StatusOfTask = Status;
                                    Status = "Просрочен";

                                }

                            }
                            else if (z == 3) { 
                                  

                        
                            Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы поставить ему действие при наступлении DeadLine : ");
                            int come = Convert.ToInt32(Console.ReadLine());
                            var ToCome = tasks.Find(r => r._idOfTask == come);
                            Console.WriteLine($"Задача <<{ToCome._NameOfTask}>> с Дедлайном в {ToCome._DeadLine}");
                          

                          
                                Console.WriteLine("Оставьте любой символ, чтобы как-то отметить задачу");
                                string symbol = Console.ReadLine();
                                Console.WriteLine($"Будет выглядеть так --> {ToCome._NameOfTask} {symbol}");
                               _ = ToCome._NameOfTask + symbol;

                            }
                            break;
                        case 4:
                            Console.WriteLine();
                            Console.WriteLine("Применение метода сортировки");
                            Console.WriteLine();
                            Console.WriteLine("Сортировка по НАЗВАНИЮ задачи");
                            Console.WriteLine();
                            var SortTask = from t in tasks
                                           orderby t._NameOfTask
                                           select t;

                            foreach (Task t in SortTask)
                                Console.WriteLine(t._NameOfTask);


                            Console.WriteLine();
                            Console.WriteLine("Сортировка по СТАТУСУ задачи");
                            Console.WriteLine();
                            var SortTaskStat = from s in tasks
                                               orderby s._StatusOfTask
                                               select s;

                            foreach (Task s in SortTaskStat)
                                Console.WriteLine(s._StatusOfTask);

                        

                            Console.WriteLine();
                            Console.WriteLine("Сортировка по ДАТЕ СОЗДАНИЯ задачи");
                            Console.WriteLine();
                            var SortTaskDate = from tusk in tasks
                                               orderby tusk._EnterDateOfTask
                                               select tusk;
                            foreach (Task tusk in SortTaskDate)
                                Console.WriteLine(tusk._EnterDateOfTask);

                            Console.WriteLine();
                            Console.WriteLine("Сортировка по ДАТЕ DEADLINE задачи");
                            Console.WriteLine();
                            var SortDeadLine = from dead in tasks
                                               orderby dead._DeadLine
                                               select dead;
                            foreach (Task dead in SortDeadLine)
                                Console.WriteLine(dead._DeadLine);
                            break;

                    }
                    Console.WriteLine();
                    Console.WriteLine("--Готово!--");
                    Console.WriteLine("Продолжить (y/n)? ");

                    again = Console.ReadLine();





                } while (again == "y");


                string jsonData = JsonConvert.SerializeObject(tasks);
            }

        }
    }
}