﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    class Program
    {
        static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        static void Main(string[] args)
        {
           
            socket.Connect("127.0.0.1", 49670);
            Console.WriteLine("Введите свое имя:");
            string message = Console.ReadLine();
            byte[] buffer = Encoding.UTF8.GetBytes(message);
            socket.Send(buffer);
            Console.WriteLine("1. Отправить на сервер символ");
            Console.WriteLine("2. Отправить на сервер время");
            Console.WriteLine("3. Отправить на сервер рандомные числа");
            int x = Int32.Parse(Console.ReadLine());
            switch (x)
            {
                case 1:
                    Console.WriteLine("Введите любой символ");
                    string smile = Console.ReadLine();
                    byte[] buffer1 = Encoding.UTF8.GetBytes(smile);
                    socket.Send(buffer1);
                    break;

                case 2:
                    byte[] buffer2 = Encoding.UTF8.GetBytes(DateTime.Now.ToString("f"));
                    socket.Send(buffer2);
                    break;
                case 3:
                    Random rand = new Random();
                    int value = rand.Next();
                  
                    byte[] buffer3 = Encoding.UTF8.GetBytes(value) ;
                    socket.Send(buffer3);
                    break;
            }

           
         

            Console.ReadLine();
        }
    }
}
