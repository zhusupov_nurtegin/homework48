﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Xml.Serialization;

namespace Server
{
    class Program
    {
        static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        static void Main(string[] args)
        {
            socket.Bind(new IPEndPoint(IPAddress.Any, 49670));
            socket.Listen(5);
            Socket client = socket.Accept();
            Console.WriteLine("Есть новое подключение!");
            byte[] buffer = new byte[1024];
            client.Receive(buffer);
            Console.WriteLine("Добро пожаловать, " + Encoding.UTF8.GetString(buffer));


            byte[] buffer1 = new byte[1024];
            client.Receive(buffer1);
            Console.WriteLine("Ваш символ:" + Encoding.UTF8.GetString(buffer1));
            Console.ReadLine();

            byte[] buffer2 = new byte[1024];
            client.Receive(buffer2);
            Console.WriteLine("Время:" + Encoding.UTF8.GetString(buffer2));

            Console.ReadLine();

        }
    }
}
