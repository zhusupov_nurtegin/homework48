﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework47.Models
{
    class AutoContext : DbContext
    {
            public DbSet<User> Users { get; set; }
            public DbSet<Car> Cars { get; set; }
        public AutoContext(DbContextOptions<AutoContext> options)

            : base(options)
        {
        }
        }
    }

