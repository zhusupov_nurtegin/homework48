use study_database

SELECT category, p.product,  FORMAT(action_date, '%d-%M-%Y') as Date, qty, a.price
    FROM actions a , products , categories

    JOIN products p ON p.id=p.id
		
    WHERE p.category_id=2 AND YEAR(a.action_date) < 2015 
	
    ORDER BY qty ASC
    