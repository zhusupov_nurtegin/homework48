﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ChatClient
{
    class Program

    {

        static string userName;
        static string password;
        static string phoneNumber;
        private const string host = "127.0.0.1";

        private const int port = 8888;

        static TcpClient client;

        static NetworkStream stream;

        static void NewLogging()
        {
            string answer = "";
            Console.Write("Введите свой номер: ");

            phoneNumber = Console.ReadLine();


            Console.Write("Введите свое имя: ");

            userName = Console.ReadLine();

            Console.Write("Введите свой пароль: ");

            password = Console.ReadLine();
            answer = $"{phoneNumber}|{userName}|{password}";

            byte[] data = Encoding.Unicode.GetBytes(answer);

            stream.Write(data, 0, data.Length);
            string Confirm = MessageGet();
            Console.WriteLine("Сервер ответил вам:" + Confirm);
            if (Confirm == "not valid")
            {
                NewLogging();
            }
        }
        static void Autorization()
        {
            int count = 1;
            while (count <= 3)
            {
                Console.WriteLine($"Попытка авторизации № {count}");
                Console.Write("Введите свое имя: ");

                userName = Console.ReadLine();

                Console.Write("Введите свой пароль: ");

                password = Console.ReadLine();

                string Autoinfo = $"{userName}|{password}";
                byte[] data = Encoding.Unicode.GetBytes(Autoinfo);
                stream.Write(data, 0, data.Length);
                count++;

                string Confirm = MessageGet();
                Console.WriteLine("Сервер ответил вам:" + Confirm);
                if (Confirm == "Пользователя с таким именем или паролем не найдено, либо данные введены неправильно. Попытайтесь еще раз")
                {
                    NewLogging();
                }
            }
            if (count == 4)
                Console.WriteLine("Попыток не осталось. Вам выдана блокировака в виде 60 минут в запое.");

            Thread.Sleep(60 *60 *1000);
            while (Console.KeyAvailable) 
               Console.ReadKey(true);






        }
        static void Main(string[] args)

        {


            client = new TcpClient();

            try

            {

                client.Connect(host, port); //подключение клиента

                stream = client.GetStream(); // получаем поток
          
                Console.WriteLine("Выберите дальнейшие действия:");
                Console.WriteLine("1- Авторизация");
                Console.WriteLine("2- Регистрация");
                string choice = Console.ReadLine();
                byte[] data = Encoding.Unicode.GetBytes(choice);

                stream.Write(data, 0, data.Length);

                if (choice == "1")
                
                    Autorization();
                
                else if (choice == "2")
                
                    NewLogging();



       

                // запускаем новый поток для получения данных
              
                IfLogged();

            }

            catch (Exception ex)

            {

                Console.WriteLine(ex.Message);

            }

            finally

            {

                Disconnect();

            }

        }
    
        static void IfLogged()
        {

            Thread receiveThread = new Thread(ReceiveMessage);

            receiveThread.Start(); //старт потока



            SendMessage();
        }
        // отправка сообщений

        static void SendMessage()

        {
            Console.WriteLine("Добро пожаловать, {0}", userName);

            Console.WriteLine("Введите сообщение: ");


            while (true)

            {

                string message = Console.ReadLine();
                string[] messageArray = message.Split(":");

                if (messageArray.Length > 1)
                {
                    message = messageArray[1];
                    userName = Convert.ToString(messageArray[0]);
                }
                

                byte[] data = Encoding.Unicode.GetBytes(message);

                stream.Write(data, 0, data.Length);

            }

        }

        // получение первого сообщения
        static string MessageGet()
        {
            byte[] data = new byte[64]; // буфер для получаемых данных

            StringBuilder builder = new StringBuilder();

            int bytes = 0;

            do

            {

                bytes = stream.Read(data, 0, data.Length);

                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));

            }

            while (stream.DataAvailable);


            return builder.ToString();
        }
        static void ReceiveMessage()

        {

            while (true)

            {

                try

                {




                    string message = MessageGet();

                    Console.WriteLine(message);//вывод сообщения

                }

                catch

                {

                    Console.WriteLine("Подключение прервано!"); //соединение было прервано

                    Console.ReadLine();

                    Disconnect();

                }

            }

        }


        static void Disconnect()

        {

            if (stream != null)

                stream.Close();//отключение потока

            if (client != null)

                client.Close();//отключение клиента

            Environment.Exit(0); //завершение процесса

        }

    }
}
