﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;

namespace ChatServer
{
    class Server

    {
        public string Id { get; set; }
        public string userName { get; set; }
        public string password { get; set; }

        static TcpListener tcpListener; // сервер для прослушивания

        List<Client> clients = new List<Client>(); // все подключения

        protected internal Client GetClientById(string id)
        {
            return clients.FirstOrDefault(c => c.Id == id);
        }

        protected internal void AddConnection(Client client)

        {

            clients.Add(client);
            Write();

        }

        protected internal void RemoveConnection(string id)

        {

            Client client = clients.FirstOrDefault(c => c.Id == id);

            if (client != null)

                clients.Remove(client);

        }


        // прослушивание входящих подключений
        public void ReplaceClients(string id, Client client2)
        {
            clients[clients.FindIndex(c => c.Id == id)] = client2;
            Write();
        }
        protected internal void Listen()

        {

            try

            {

                tcpListener = new TcpListener(IPAddress.Any, 8888);

                tcpListener.Start();

                Console.WriteLine("Сервер запущен. Ожидание подключений...");


                while (true)

                {

                    TcpClient tcpClient = tcpListener.AcceptTcpClient();


                    Client client = new Client(tcpClient, this);

                    Thread clientThread = new Thread(client.Process);

                    clientThread.Start();

                }

            }

            catch (Exception ex)

            {

                Console.WriteLine(ex.Message);

                Disconnect();

            }

        }




        protected internal void BroadcastMessage(string message, string id)

        {

            byte[] data = Encoding.Unicode.GetBytes(message);

            for (int i = 0; i < clients.Count; i++)

            {

                if (clients[i].Id != id && clients[i].isOnline()) // если id клиента не равно id отправляющего

                {

                    clients[i].Stream.Write(data, 0, data.Length); //передача данных

                }

            }

        }



        protected internal void Disconnect()

        {

            tcpListener.Stop(); //остановка сервера


            for (int i = 0; i < clients.Count; i++)

            {

                clients[i].Close(); //отключение клиента

            }

            Environment.Exit(0); //завершение процесса

        }
        public List<Client> ReadSomeString()
        {
            if (clients.Count == 0)
            {
                string json = File.ReadAllText("../../../data/clients.json");
                Console.WriteLine(json);
                byte[] EncodeBytes = Encoding.UTF8.GetBytes(json);
                Encoding.Convert(Encoding.UTF8, Encoding.Unicode, EncodeBytes);
                clients = JsonConvert.DeserializeObject<List<Client>>(json);
            }
            return clients;
        } 

        public void Write()
        {
            var str = JsonConvert.SerializeObject(clients, new Formatting());
            File.WriteAllText("../../../data/clients.json", str);
        }
        public List<Client> ReadFromClients()
        {
            if (clients.Count == 0)
            {
                string jsonString = File.ReadAllText("../../../data/clients.json");

                byte[] EncodeBytes = Encoding.UTF8.GetBytes(jsonString);
                Encoding.Convert(Encoding.UTF8, Encoding.Unicode, EncodeBytes);
                clients = JsonConvert.DeserializeObject<List<Client>>(jsonString);
            }
            return clients;
        }
       
    }
}
