
CREATE TABLE brands (
  id int NOT NULL IDENTITY(1, 1),
  brand nvarchar(45) NOT NULL,
  PRIMARY KEY (id)
)



CREATE TABLE categories (
  id INT NOT NULL IDENTITY(1, 1),
   income_qtyy int not null  ,
  category nvarchar(45) NOT NULL DEFAULT 'name category',
  description nvarchar(100) DEFAULT NULL,
  PRIMARY KEY (id)
)


CREATE TABLE suppliers (
  id INT NOT NULL IDENTITY(1, 1),
  supplier nVARCHAR(50) NOT NULL,
  income_sum int NOT NULL,
  address nVARCHAR(50),
  contacts nVARCHAR(50),
  PRIMARY KEY (id),
      )  
  Select sum(Income_sum)
  from suppliers 


CREATE TABLE products (
  id int NOT NULL IDENTITY(1, 1),
  product nvarchar(45) NOT NULL,
  price decimal DEFAULT '0.00',
  description varchar(100) DEFAULT NULL,
  PRIMARY KEY (id),
   action_sum int not null,
   action_qty int not null,
  category_id INT NOT NULL FOREIGN KEY 
	REFERENCES categories (id) ON UPDATE CASCADE,
  brand_id INT FOREIGN KEY
	REFERENCES brands (id) ON UPDATE CASCADE
)

CREATE TABLE actions (
  id int NOT NULL IDENTITY(1, 1) Primary key,
  action_date datetime NOT NULL,
  Summa int,
  product_id int  NOT NULL,
  supplier_id int NOT NULL,
  qty decimal NOT NULL DEFAULT '0.000',
  price decimal NOT NULL DEFAULT '0.00',



 
  CONSTRAINT FK_product FOREIGN KEY  (product_id) REFERENCES products (id),
  CONSTRAINT FK_supplier FOREIGN KEY (supplier_id) REFERENCES suppliers (id))

  




INSERT INTO categories (category, description,income_qtyy) VALUES ('HDD', '������� �����',40);
INSERT INTO categories (category, description,income_qtyy) VALUES ('Monitors', '��������',30);
INSERT INTO categories (category, description,income_qtyy) VALUES ('Motherboards', '����������� �����',40);
INSERT INTO categories (category, description,income_qtyy) VALUES ('DDR', '����������� ������',80);



INSERT INTO brands (brand) VALUES ('Samsung');
INSERT INTO brands (brand) VALUES ('ASUS');
INSERT INTO brands (brand) VALUES ('Acer');


INSERT INTO suppliers (supplier, address,income_sum,  contacts) VALUES ('Silk Road','Shanghai 6-7/115',4005,'mr.Lee p:234-177-123-456-6');
INSERT INTO suppliers (supplier, address, income_sum, contacts) VALUES ('Cargo Transfer','Shanghai, Mao av. 899-1201',4001, Null);
INSERT INTO suppliers (supplier, address,income_sum,  contacts) VALUES ('4U','Urumchi',4009,'');
INSERT INTO suppliers (supplier, address,income_sum,  contacts) VALUES ('IDT','Beijing',4002,'');


INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='HDD'),
(SELECT id FROM brands where brand='Samsung'),
'S1 HDD 1T, 7200 rpm', 125, 340, 220 ,'');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='HDD'),
(SELECT id FROM brands where brand='Samsung'),
'S2 HDD 500Gb, 7200 rpm', 85,340, 220 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='HDD'),
(SELECT id FROM brands where brand='Samsung'),
'S3 HDD 250Gb, 10000 rpm', 150, 340, 220 ,'');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='HDD'),
(SELECT id FROM brands where brand='Samsung'),
'S4 HDD 1T, 7200rpm', 127,340, 220 , '');

INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='HDD'),
(SELECT id FROM brands where brand='Samsung'),
'S5 HDD 500Gb, 7200rpm', 92,340, 220 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='HDD'),
(SELECT id FROM brands where brand='Acer'),
'Z4 HDD 480Gb, 78000rpm', 989,340, 220 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='HDD'),
(SELECT id FROM brands where brand='Acer'),
'Z2 HDD 750Gb, 72000rpm', 110, 340, 220 ,'');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='HDD'),
NULL, 'UN1 HDD 750Gb, 72000rpm', 105,340, 220 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Monitors'),
(SELECT id FROM brands where brand='Samsung'),
NULL, 'PZ4 HDD 750Gb, 68000rpm', 228,340, 400 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Monitors'),
(SELECT id FROM brands where brand='Samsung'),
'Monitor B22', 170,340, 333 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Monitors'),
(SELECT id FROM brands where brand='Samsung'),
'Monitor B25', 320, 340, 513 ,'');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Monitors'),
(SELECT id FROM brands where brand='Acer'),
'Monitor A20', 150,340, 212 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Monitors'),
(SELECT id FROM brands where brand='Acer'),
'Monitor A21', 170, 340, 190 ,'');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Monitors'),
(SELECT id FROM brands where brand='ASUS'),
'Monitor DD27', 370,340, 290 , '');

INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='DDR'),
(SELECT id FROM brands where brand='Samsung'),
'DDR3 SST 2Gb', 25,340, 220 , '');

INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='DDR'),
(SELECT id FROM brands where brand='Samsung'),
'DDR3 SST 4Gb', 42,340, 220 , '');
-- ������� ������ 17
INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='DDR'),
(SELECT id FROM brands where brand='ASUS'),
'DDR3 BTT 4Gb', 48,340, 220 , '');
-- ������� ������ 18
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='DDR'),
(SELECT id FROM brands where brand='ASUS'),
'DDR3 BTT 8Gb', 90,340, 220 , '');
-- ������� ������ 19
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='DDR'),
(SELECT id FROM brands where brand='ASUS'),
'DDR3 ZPP 4Gb', 45,340, 220 , '');
-- ������� ������ 20
INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='DDR'),
NULL,'DDR3 UNK 2Gb', 24,340, 220 , '');
-- ������� ������ 21
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='DDR'),
NULL,'DDR2 UNK 1Gb', 17,340, 220 , '');
-- ������� ������ 22
INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
(SELECT id FROM brands where brand='Samsung'),
'MB-1', 75,340, 220 , '');
-- ������� ������ 23
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
(SELECT id FROM brands where brand='Samsung'),
'MB-2', 76, 340, 220 ,'');
-- ������� ������ 24
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
(SELECT id FROM brands where brand='ASUS'),
'MB-3', 80, 340, 220 ,'');
-- ������� ������ 25
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
(SELECT id FROM brands where brand='ASUS'),
'MB-4', 95, 340, 220 ,'');
-- ������� ������ 26
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
(SELECT id FROM brands where brand='ASUS'),
'MB-5', 160, 340, 220 ,'');
-- ������� ������ 27
INSERT INTO products (category_id, brand_id, product, price,action_sum, action_qty, description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
(SELECT id FROM brands where brand='Acer'),
'MB-6', 180, 340, 220 ,'');
-- ������� ������ 28
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
(SELECT id FROM brands where brand='Acer'),
'MB-7', 200, 340, 220 ,'');
-- ������� ������ 29
INSERT INTO products (category_id, brand_id, product, price, action_sum, action_qty,description)
VALUES ((SELECT id FROM categories where category='Motherboards'),
NULL, 'MB-8', 250,340, 220 , '');

-- Silk Road
-- ������� ����������� ������ actions 1
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-25 12:30:33', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
100, -- qty
110);
-- ������� ����������� ������ actions 2
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-25 12:30:33', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
80, -- qty
95);
-- ������� ����������� ������ actions 3
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-25 12:30:33', 
(select id from products where product='UN1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
48, -- qty
84);
-- ������� ����������� ������ actions 4
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-25 12:30:33', 
(select id from products where product='Monitor B25'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
16, -- qty
340);
-- ������� ����������� ������ actions 5
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-30 09:30:00', 
(select id from products where product='Monitor B25'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
10, -- qty
345);
-- ������� ����������� ������ actions 6
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-30 09:30:00', 
(select id from products where product='DDR3 SST 2Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
50, -- qty
40);
-- ������� ����������� ������ actions 7
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-30 09:30:00', 
(select id from products where product='DDR3 SST 4Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
40, -- qty
55);
-- ������� ����������� ������ actions 8
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-14 15:30:00', 
(select id from products where product='DDR3 BTT 4Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
50, -- qty
60);
-- ������� ����������� ������ actions 9
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-14 15:30:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
50, -- qty
98);
-- ������� ����������� ������ actions 10
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-20 10:05:00', 
(select id from products where product='Monitor DD27'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
10, -- qty
330);
-- 13.02.2017
-- ������� ����������� ������ actions 11
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-25 12:15:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
50, -- qty
115);
-- ������� ����������� ������ actions 12
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-25 12:15:00', 
(select id from products where product='DDR3 BTT 4Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
15, -- qty
86);
-- ������� ����������� ������ actions 13
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-25 12:15:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
25, -- qty
117);
-- ������� ����������� ������ actions 14
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-25 14:33:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
25, -- qty
117);
-- ������� ����������� ������ actions 15
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-10-04 12:50:00', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
30, -- qty
121);
-- ������� ����������� ������ actions 16
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-10-27 09:18:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
5, -- qty
125);
-- ������� ����������� ������ actions 17
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-12-11 09:20:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
10, -- qty
143);
-- ������� ����������� ������ actions 18
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-07 09:43:00', 
(select id from products where product='DDR3 BTT 4Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
51, -- qty
43);
-- ������� ����������� ������ actions 19
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-03-12 09:21:00', 
(select id from products where product='DDR3 SST 2Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
70, -- qty
18);
-- ������� ����������� ������ actions 20
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-06-28 08:55:00', 
(select id from products where product='DDR3 SST 4Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
35, -- qty
36);
-- ������� ����������� ������ actions 21
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-15 10:02:00', 
(select id from products where product='Monitor A20'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
20, -- qty
135);
-- ������� ����������� ������ actions 22
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-16 09:57:00', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
45, -- qty
116);
-- ������� ����������� ������ actions 23
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-09-15 09:15:00', 
(select id from products where product='Monitor A20'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
5, -- qty
142);
-- ������� ����������� ������ actions 24
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-09-20 10:03:00', 
(select id from products where product='S2 HDD 500Gb, 7200 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
13, -- qty
74);
-- ������� ����������� ������ actions 25
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-02 11:10:00', 
(select id from products where product='Monitor B19'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
24, -- qty
121);
-- ������� ����������� ������ actions 26
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-23 10:02:00', 
(select id from products where product='DDR3 BTT 8Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
55, -- qty
79);
-- ������� ����������� ������ actions 27
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-23 10:03:00', 
(select id from products where product='Monitor B25'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
6, -- qty
295);
-- ������� ����������� ������ actions 28
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-23 10:04:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
100, -- qty
108);
-- ������� ����������� ������ actions 29
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-11-11 09:44:00', 
(select id from products where product='DDR3 ZPP 4Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
75, -- qty
31);
-- ������� ����������� ������ actions 30
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-12-17 11:00:00', 
(select id from products where product='DDR3 BTT 8Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
20, -- qty
77);
-- ������� ����������� ������ actions 31
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-12-20 08:50:00', 
(select id from products where product='DDR3 BTT 4Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
12, -- qty
40);
-- ������� ����������� ������ actions 32
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-01-12 11:01:00', 
(select id from products where product='Monitor DD27'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
10, -- qty
340);
-- ������� ����������� ������ actions 33
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-01-12 11:02:00', 
(select id from products where product='DDR3 BTT 8Gb'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
25, -- qty
76);
-- ������� ����������� ������ actions 34
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-01-12 11:03:00 ', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
37, -- qty
115);
-- ������� ����������� ������ actions 35
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-25 13:50:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
50, -- qty
128);
-- ������� ����������� ������ actions 36
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-26 09:30:00', 
(select id from products where product='Monitor B25'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
19, -- qty
302);
-- ������� ����������� ������ actions 37
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-27 09:36:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
11, -- qty
119);
-- ������� ����������� ������ actions 38
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-03-02 09:44:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='Silk Road'), -- supplier
40, -- qty
80);

-- Cargo Transfer
-- ������� ����������� ������ actions 39
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-26 08:30:00', 
(select id from products where product='MB-1'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
25, -- qty
76);
-- ������� ����������� ������ actions 40
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-26 08:30:00', 
(select id from products where product='MB-2'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
15, -- qty
79);
-- ������� ����������� ������ actions 41
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-26 08:30:00', 
(select id from products where product='MB-3'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
20, -- qty
85);
-- ������� ����������� ������ actions 42
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-26 08:30:00', 
(select id from products where product='MB-4'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
23, -- qty
88);
-- ������� ����������� ������ actions 43
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-26 08:30:00', 
(select id from products where product='MB-5'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
10, -- qty
94);
-- ������� ����������� ������ actions 44
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-28 08:45:00', 
(select id from products where product='MB-2'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
10, -- qty
79);
-- ������� ����������� ������ actions 45
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-28 08:45:00', 
(select id from products where product='MB-4'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
7, -- qty
89);
-- ������� ����������� ������ actions 46
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-07 08:45:00', 
(select id from products where product='MB-1'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
30, -- qty
76);
-- ������� ����������� ������ actions 47
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-07 08:45:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
24, -- qty
105);
-- ������� ����������� ������ actions 48
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-13 11:12:00', 
(select id from products where product='MB-1'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
10, -- qty
75);
-- ������� ����������� ������ actions 49
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-13 11:12:00', 
(select id from products where product='MB-4'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
25, -- qty
77);
-- 13.02.2017
-- ������� ����������� ������ actions 50
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-11-20 12:30:00', 
(select id from products where product='MB-7'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
17, -- qty
181);
-- ������� ����������� ������ actions 51
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-11-20 12:31:00', 
(select id from products where product='MB-6'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
6, -- qty
168);
-- ������� ����������� ������ actions 52
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-04-03 11:25:00', 
(select id from products where product='MB-2'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
50, -- qty
57);
-- ������� ����������� ������ actions 53
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-04-24 10:35:00', 
(select id from products where product='MB-3'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
60, -- qty
63);
-- ������� ����������� ������ actions 54
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-03 10:45:00', 
(select id from products where product='MB-6'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
11, -- qty
169);
-- ������� ����������� ������ actions 55
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-07 13:07:00', 
(select id from products where product='MB-2'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
20, -- qty
70);
-- ������� ����������� ������ actions 56
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-07 13:08:00', 
(select id from products where product='MB-5'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
25, -- qty
144);
-- ������� ����������� ������ actions 57
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-07 13:09:00', 
(select id from products where product='MB-4'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
40, -- qty
81);
-- ������� ����������� ������ actions 58
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-16 14:02:00', 
(select id from products where product='MB-8'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
10, -- qty
236);
-- ������� ����������� ������ actions 59
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-08-12 11:37:00', 
(select id from products where product='MB-7'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
12, -- qty
180);
-- ������� ����������� ������ actions 60
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-08-20 10:04:00', 
(select id from products where product='MB-2'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
30, -- qty
68);
-- ������� ����������� ������ actions 61
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-09-11 11:14:00', 
(select id from products where product='MB-3'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
100, -- qty
62);
-- ������� ����������� ������ actions 62
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-06 09:57:00', 
(select id from products where product='S2 HDD 500Gb, 7200 rpm'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
10, -- qty
74);
-- ������� ����������� ������ actions 63
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-06 09:59:00', 
(select id from products where product='MB-6'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
19, -- qty
159);
-- ������� ����������� ������ actions 64
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-7 13:20:00', 
(select id from products where product='MB-2'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
11, -- qty
77);
-- ������� ����������� ������ actions 65
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-16 09:48:00', 
(select id from products where product='MB-4'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
3, -- qty
91);
-- ������� ����������� ������ actions 66
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-19 10:06:00', 
(select id from products where product='MB-7'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
8, -- qty
188);
-- ������� ����������� ������ actions 67
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-10-27 10:45:00', 
(select id from products where product='MB-3'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
20, -- qty
72);
-- ������� ����������� ������ actions 68
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-25 11:12:00', 
(select id from products where product='MB-1'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
25, -- qty
63);
-- ������� ����������� ������ actions 69
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-03-03 14:25:00', 
(select id from products where product='MB-8'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
15, -- qty
234);
-- ������� ����������� ������ actions 70
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-03-06 09:52:00', 
(select id from products where product='S2 HDD 500Gb, 7200 rpm'), -- product
(select id from suppliers where supplier='Cargo Transfer'), -- supplier
25, -- qty
76);

-- 4U
-- ������� ����������� ������ actions 71
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-25 09:05:00', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
33, -- qty
115);
-- ������� ����������� ������ actions 72
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-25 09:05:00', 
(select id from products where product='Z1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
40, -- qty
90);
-- ������� ����������� ������ actions 73
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-26 16:45:00', 
(select id from products where product='UN1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
100, -- qty
94);
-- ������� ����������� ������ actions 74
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-28 09:10:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
80, -- qty
86);
-- ������� ����������� ������ actions 75
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-03 14:15:00', 
(select id from products where product='DDR3 SST 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
100, -- qty
34);
-- ������� ����������� ������ actions 76
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-03 14:15:00', 
(select id from products where product='Monitor DD27'), -- product
(select id from suppliers where supplier='4U'), -- supplier
10, -- qty
345);
-- ������� ����������� ������ actions 77
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-13 10:00:00', 
(select id from products where product='Monitor DD27'), -- product
(select id from suppliers where supplier='4U'), -- supplier
5, -- qty
345);
-- ������� ����������� ������ actions 78
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-13 10:00:00', 
(select id from products where product='MB-8'), -- product
(select id from suppliers where supplier='4U'), -- supplier
30, -- qty
235);
-- ������� ����������� ������ actions 79
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-15 17:15:00', 
(select id from products where product='DDR3 BTT 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
100, -- qty
39);
-- ������� ����������� ������ actions 80
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-16 09:45:00', 
(select id from products where product='MB-7'), -- product
(select id from suppliers where supplier='4U'), -- supplier
50, -- qty
180);
-- ������� ����������� ������ actions 81
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-16 09:45:00', 
(select id from products where product='Monitor DD27'), -- product
(select id from suppliers where supplier='4U'), -- supplier
12, -- qty
350);
-- 13.02.2017
-- ������� ����������� ������ actions 82
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-12-19 09:23:00', 
(select id from products where product='Z1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
50, -- qty
79);
-- ������� ����������� ������ actions 83
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-01-07 10:10:00', 
(select id from products where product='Z2 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
30, -- qty
94);
-- ������� ����������� ������ actions 84
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-01-09 10:23:00', 
(select id from products where product='DDR3 UNK 2Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
25, -- qty
18);
-- ������� ����������� ������ actions 85
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-01-23 15:45:00', 
(select id from products where product='DDR3 BTT 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
60, -- qty
39);
-- ������� ����������� ������ actions 86
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-01-25 13:17:00', 
(select id from products where product='Z1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
5, -- qty
101);
-- ������� ����������� ������ actions 87
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-01 09:45:00', 
(select id from products where product='Monitor DD27'), -- product
(select id from suppliers where supplier='4U'), -- supplier
5, -- qty
351);
-- ������� ����������� ������ actions 88
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-25 09:15:00', 
(select id from products where product='MB-8'), -- product
(select id from suppliers where supplier='4U'), -- supplier
20, -- qty
225);
-- ������� ����������� ������ actions 89
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-25 10:03:00', 
(select id from products where product='UN1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
16, -- qty
93);
-- ������� ����������� ������ actions 90
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-03-09 17:43:00', 
(select id from products where product='DDR3 BTT 8Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
24, -- qty
75);
-- ������� ����������� ������ actions 91
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-03-18 13:06:00', 
(select id from products where product='MB-7'), -- product
(select id from suppliers where supplier='4U'), -- supplier
10, -- qty
183);
-- ������� ����������� ������ actions 92
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-03-24 11:18:00', 
(select id from products where product='Z1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
40, -- qty
92);
-- ������� ����������� ������ actions 93
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-03-24 11:19:00', 
(select id from products where product='Z2 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
35, -- qty
99);
-- ������� ����������� ������ actions 94
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-03-24 11:20:00', 
(select id from products where product='DDR3 ZPP 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
20, -- qty
37);
-- ������� ����������� ������ actions 95
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-04-12 15:55:00', 
(select id from products where product='UN1 HDD 750Gb, 72000rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
50, -- qty
95);
-- ������� ����������� ������ actions 96
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-05-18 13:15:00', 
(select id from products where product='MB-8'), -- product
(select id from suppliers where supplier='4U'), -- supplier
1, -- qty
255);
-- ������� ����������� ������ actions 97
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-05-18 13:15:00', 
(select id from products where product='MB-7'), -- product
(select id from suppliers where supplier='4U'), -- supplier
3, -- qty
196);
-- ������� ����������� ������ actions 98
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-06-01 10:05:00', 
(select id from products where product='DDR3 ZPP 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
60, -- qty
34);
-- ������� ����������� ������ actions 99
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-01 12:21:00', 
(select id from products where product='Monitor DD27'), -- product
(select id from suppliers where supplier='4U'), -- supplier
7, -- qty
348);
-- ������� ����������� ������ actions 100
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-12-13 09:52:00', 
(select id from products where product='DDR3 ZPP 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
10, -- qty
42);
-- ������� ����������� ������ actions 101
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-01-20 15:35:00', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='4U'), -- supplier
20, -- qty
115);
-- ������� ����������� ������ actions 102
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-25 16:05:00', 
(select id from products where product='DDR3 SST 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
80, -- qty
36);
-- ������� ����������� ������ actions 103
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-03-12 12:45:00', 
(select id from products where product='DDR3 BTT 4Gb'), -- product
(select id from suppliers where supplier='4U'), -- supplier
50, -- qty
39);

-- IDT
-- ������� ����������� ������ actions 104
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-29 13:00:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
120, -- qty
78);
-- ������� ����������� ������ actions 105
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-07-29 13:00:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
80, -- qty
130);
-- ������� ����������� ������ actions 106
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-10 09:50:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
60, -- qty
78);
-- ������� ����������� ������ actions 107
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2015-08-12 10:10:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
85, -- qty
129);
-- 13.02.2017
-- ������� ����������� ������ actions 108
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-12 09:15:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
100, -- qty
127);
-- ������� ����������� ������ actions 109
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-25 17:02:00', 
(select id from products where product='S2 HDD 500Gb, 7200 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
50, -- qty
73);
-- ������� ����������� ������ actions 110
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-25 17:03:00', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
63, -- qty
99);
-- ������� ����������� ������ actions 111
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-02-25 17:03:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
20, -- qty
82);
-- ������� ����������� ������ actions 112
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-06-14 16:03:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
25, -- qty
127);
-- ������� ����������� ������ actions 113
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-07-19 10:25:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
20, -- qty
127);
-- ������� ����������� ������ actions 114
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-09-01 09:25:00', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
30, -- qty
116);
-- ������� ����������� ������ actions 115
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-11-01 10:10:00', 
(select id from products where product='S2 HDD 500Gb, 7200 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
30, -- qty
73);
-- ������� ����������� ������ actions 116
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2016-12-25 13:09:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
20, -- qty
83);
-- ������� ����������� ������ actions 117
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-01-11 09:15:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
15, -- qty
120);
-- ������� ����������� ������ actions 118
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-01-14 10:22:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
90, -- qty
127);
-- ������� ����������� ������ actions 119
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-01-29 10:30:00', 
(select id from products where product='S2 HDD 500Gb, 7200 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
40, -- qty
77);
-- ������� ����������� ������ actions 120
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-10 15:56:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
20, -- qty
118);
-- ������� ����������� ������ actions 121
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-10 15:57:00', 
(select id from products where product='S1 HDD 1T, 7200 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
25, -- qty
116);
-- ������� ����������� ������ actions 122
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-22 11:37:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
65, -- qty
80);
-- ������� ����������� ������ actions 123
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-02-28 08:47:00', 
(select id from products where product='S3 HDD 250Gb, 10000 rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
10, -- qty
145);
-- ������� ����������� ������ actions 124
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-03-05 10:45:00', 
(select id from products where product='S4 HDD 1T, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
100, -- qty
109);
-- ������� ����������� ������ actions 125
INSERT INTO actions (action_date, product_id, supplier_id, qty, price)
VALUES ('2017-03-10 13:23:00', 
(select id from products where product='S5 HDD 500Gb, 7200rpm'), -- product
(select id from suppliers where supplier='IDT'), -- supplier
45, -- qty
84);
ALTER TABLE suppliers
ADD income_sum DECIMAL(10,2) NOT NULL DEFAULT 0;
UPDATE suppliers
SET income_sum = (SELECT sum(price*qty)
FROM actions
WHERE supplier_id = suppliers.id);

ALTER TABLE dbo.products
ADD income_qty Decimal(10, 3) NOT NULL DEFAULT 0
ALTER TABLE dbo.products
ADD income_sum Decimal(10, 3) NOT NULL DEFAULT 0


