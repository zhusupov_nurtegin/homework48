use study_database;
GO  
IF OBJECT_ID('ProductInfo') IS NOT NULL  
   DROP PROCEDURE ProductInfo;  
GO  
CREATE PROCEDURE ProductInfo
AS    
 
DECLARE @NameOfProduct nvarchar
SELECT @NameOfProduct = product
FROM products
RETURN @NameOfProduct;

Declare @resultPro nvarchar

Exec @resultPro = ProductInfo
print @resultPro