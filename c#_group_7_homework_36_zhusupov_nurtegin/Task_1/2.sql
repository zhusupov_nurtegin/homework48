use study_database;
GO  
IF OBJECT_ID('CategoryInfo') IS NOT NULL  
   DROP PROCEDURE CategoryInfo;  
GO  
CREATE PROCEDURE CategoryInfo
AS    
 
DECLARE @NameOfCategory nvarchar
SELECT @NameOfCategory = category
FROM categories
RETURN @NameOfCategory;

Declare @resultCat nvarchar

Exec @resultCat = CategoryInfo
print @resultCat