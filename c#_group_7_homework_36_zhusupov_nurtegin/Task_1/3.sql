use study_database;
GO  
IF OBJECT_ID('INFO') IS NOT NULL  
   DROP PROCEDURE INFO;  
GO  
CREATE PROCEDURE INFO 
AS    
 
DECLARE @NameOfBrand nvarchar
SELECT @NameOfBrand = brand
FROM brands
RETURN @NameOfBrand;

Declare @result nvarchar

Exec @result = INFO
print @result