USE [master]
GO

/****** Object:  Database [����� ������]    Script Date: 15.07.2020 14:50:02 ******/
CREATE DATABASE [����� ������]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'����� ������', FILENAME = N'C:\Users\ASUS_STRIX\����� ������.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'����� ������_log', FILENAME = N'C:\Users\ASUS_STRIX\����� ������_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [����� ������].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [����� ������] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [����� ������] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [����� ������] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [����� ������] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [����� ������] SET ARITHABORT OFF 
GO

ALTER DATABASE [����� ������] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [����� ������] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [����� ������] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [����� ������] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [����� ������] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [����� ������] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [����� ������] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [����� ������] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [����� ������] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [����� ������] SET  DISABLE_BROKER 
GO

ALTER DATABASE [����� ������] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [����� ������] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [����� ������] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [����� ������] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [����� ������] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [����� ������] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [����� ������] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [����� ������] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [����� ������] SET  MULTI_USER 
GO

ALTER DATABASE [����� ������] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [����� ������] SET DB_CHAINING OFF 
GO

ALTER DATABASE [����� ������] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [����� ������] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [����� ������] SET  READ_WRITE 
GO

USE [����� ������]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Main](
[ChangeID] [int] IDENTITY(1,1) NOT NULL,
[Task] [nvarchar](100) NOT NULL,
[ComingTime][datetime]NOT NULL,
PRIMARY KEY CLUSTERED
(
[ChangeID] ASC 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
CREATE TABLE [dbo].[Workers](
    [WorkerID] [int] IDENTITY(1,1) NOT NULL,
	[ChangeID] [int] IDENTITY(10,99) NOT NULL,
    [First_Name] [nvarchar](100) NOT NULL,
	[Last_Name] [nvarchar](100) NOT NULL,
    [Sex] [nvarchar](100) NOT NULL,
    [Birthday] [datetime] NOT NULL,
    [Salary] [decimal](16, 2) NOT NULL,
	[Boss_Name] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
    [WorkerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Main]  ON 

INSERT [dbo].[Main] ([ChangeID] , [Task] , [ComingTime]) VALUES (440, N'���������� ���������' , '15:30:00')
INSERT [dbo].[Main] ([ChangeID] , [Task] , [ComingTime]) VALUES (441, N'�������� �������' , '12:45:00')
INSERT [dbo].[Main] ([ChangeID] , [Task] , [ComingTime]) VALUES (442, N'������ ����������' , '9:30:00')
INSERT [dbo].[Main] ([ChangeID] , [Task] , [ComingTime]) VALUES (443, N'������ �����' , '17:30:00')
INSERT [dbo].[Main] ([ChangeID] , [Task] , [ComingTime]) VALUES (444, N'������ � ��������' , '10:30:00')
INSERT [dbo].[Main] ([ChangeID] , [Task] , [ComingTime]) VALUES (445, N'����� � �������' , '14:50:00')
INSERT [dbo].[Main] ([ChangeID] , [Task] , [ComingTime]) VALUES (446, N'����������� ������' , '19:30:00')



SET IDENTITY_INSERT [dbo].[Main]  OFF

SET IDENTITY_INSERT [dbo].[Workers] ON

INSERT INTO [dbo].[Workers] ([WorkerID], [ChangeID], [First_Name] , [Last_Name] , [Sex] , [Salary] , [Boss_Name]) VALUES 
(1, 440 , '�����', '��������' , '�', 28000, '���� �������'),
(2, 440 , '�����', '�������' , '�', 31000, '����� ���������'),
(3, 440 , '���������', '��������' , '�', 27000, '���� �������'),
(4, 440 , '�������', '�������' , '�', 29500, '���� �������'),
(5, 440 , '�������', '������' , '�', 29000, '���� �������'),
(6, 440 , '����', '������' , '�', 25000, '���� ���������'),
(7, 440 , '������', '�������' , '�', 22000, '���� ���������')



SET IDENTITY_INSERT [dbo].[Workers] OFF

USE [master]
GO
ALTER DATABASE [Murano] SET  READ_WRITE 
GO
