﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;

namespace ChatServer
{
    class Program

    {

        static Server server; // сервер

        static Thread listenThread; // потока для прослушивания


        static void Main(string[] args)

        {
           

            try

            {

                server = new Server();
               
                server.ReadFromClients();
              
                
                listenThread = new Thread(server.Listen);

                listenThread.Start(); //старт потока

            }

            catch (Exception ex)

            {

                server.Disconnect();

                Console.WriteLine(ex.Message);

            }

        }

    }
}