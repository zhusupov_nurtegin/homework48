﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Net.Sockets;
using System.Text;

namespace ChatServer
{
    class Client

    {

        protected internal string id { get; private set; }

        protected internal NetworkStream Stream { get; private set; }
       public List<string> MyContactsId;
        string userName;
        string password;
        bool online = false;
        TcpClient tcpClient;

        Server server; // объект сервера
       
            public string Id
            {
                get { return id; }
                set { id = value; }
            }
            public string UserName
            {
                get { return userName; }
                set { userName = value; }
            }
            public string Password
            {
                get { return password; }
                set { password = value; }
            }
  
        public Client(TcpClient tcpClient, Server server)

        {

            //id = Guid.NewGuid().ToString();

            this.tcpClient = tcpClient;

            this.server = server;

            

        }
   
        public bool isOnline()
        {
            return online;
        }
        public void AddContact(string strContact)
        {
            MyContactsId.Add(strContact);
        }
        void NewLogging()
        {
            string message = GetMessage();
            string[] Array = message.Split("|");
         

            Client CheckClient = server.GetClientById(Array[0]);

            if (CheckClient == null)
            {
                Id = Array[0];
                userName = Array[1];
                password = Array[2];
                online = true;
                server.AddConnection(this);
                byte[] data = Encoding.Unicode.GetBytes("Вы успешно зарегистрировались!");
                this.Stream.Write(data, 0, data.Length);
            }
            else
            {
                byte[] data = Encoding.Unicode.GetBytes("not valid");
                this.Stream.Write(data, 0, data.Length);
                NewLogging(); 
            }

        }

        void Autorization()
        {
            string Autoinfo = GetMessage();
            string[] autoArray = Autoinfo.Split("|");
            Client CheckAcc = server.GetClientById(autoArray[0]);
            if (CheckAcc != null && CheckAcc.Password== autoArray[1])
            {
                Id = CheckAcc.Id;
                userName = CheckAcc.UserName;
                password = CheckAcc.Password;
                this.online = true;
                server.ReplaceClients(autoArray[0], this);
                byte[] data = Encoding.Unicode.GetBytes("Вы успешно авторизовались!");
                this.Stream.Write(data, 0, data.Length);
            }
            else
            {
                byte[] data = Encoding.Unicode.GetBytes("Логин или пароль введены неправильно, либо нет такого пользователя с этими данными.");
                this.Stream.Write(data, 0, data.Length);
                Autorization();
            }
        }

        public void Process()

        {

            try

            {

                Stream = tcpClient.GetStream();
                string clientActions = GetMessage();
                if (clientActions == "1")
                {
                    Autorization();
                }else if(clientActions == "2")
                {
                    NewLogging();
                }
                else
                {
                    Process();
                }
                // получаем имя пользователя
               


               string message = userName + " вошел в чат";

                // посылаем сообщение о входе в чат всем подключенным пользователям

                server.BroadcastMessage(message, this.Id);

                Console.WriteLine(message);

                // в цикле получаем сообщения от клиента

                while (true)

                {

                    try

                    {

                        message = GetMessage();

                        message = String.Format("{0}: {1}", userName, message);

                        Console.WriteLine(message);

                        server.BroadcastMessage(message, this.Id);

                    }

                    catch

                    {

                        message = String.Format("{0}: покинул чат", userName);

                        Console.WriteLine(message);

                        server.BroadcastMessage(message, this.Id);

                        break;

                    }

                }

            }

            catch (Exception e)

            {

                Console.WriteLine(e.Message);

            }

            finally

            {

                // в случае выхода из цикла закрываем ресурсы

                server.RemoveConnection(this.Id);

                Close();

            }

        }


        // чтение входящего сообщения и преобразование в строку

        private string GetMessage()

        {

            byte[] data = new byte[64]; // буфер для получаемых данных

            StringBuilder builder = new StringBuilder();

            int bytes = 0;

            do

            {

                bytes = Stream.Read(data, 0, data.Length);

                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));

            }

            while (Stream.DataAvailable);


            return builder.ToString();

        }


        // закрытие подключения

        protected internal void Close()

        {

            if (Stream != null)

                Stream.Close();

            if (tcpClient != null)

                tcpClient.Close();

        }
      
    }
}
