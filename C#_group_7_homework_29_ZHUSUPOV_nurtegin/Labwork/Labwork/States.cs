﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Labwork
{
    interface  IComparable
    {
        string New(Task task);

        string InProcess(Task task);

        string Done(Task task);

    }
}
