﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Labwork
{
   abstract class NewState : IComparable
    {
       public abstract string New(Task task);
       
       public abstract string InProcess(Task task);

      public abstract  string Done(Task task);
    }
}
