﻿using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Labwork
{
    public class Task
    {
        public int _idOfTask;
        public string _NameOfTask;
        public string _StatusOfTask;
        public int _PriorityOfTask;
        public DateTime _EnterDateOfTask;
        public DateTime _DeadLine;

        public Task(int id, string Name, string Status, int Priority, DateTime date, DateTime Deaddate)
        {
            this._idOfTask = id;
            this._NameOfTask = Name;
            this._StatusOfTask = Status;
            this._PriorityOfTask = Priority;
            this._EnterDateOfTask = date;
            this._DeadLine = Deaddate;
        }
        public Task()
        {

        }
     

        public static void Sort()
        {
            Sort();
        }

       

        public void PrintTask()
        {

            Console.WriteLine("--------------");
            Console.WriteLine($"ID задачи : {_idOfTask}");
            Console.WriteLine($"Название задачи : {_NameOfTask}");
            Console.WriteLine($"Статус задачи : {_StatusOfTask}");
            Console.WriteLine($"Приоритет задачи : {_PriorityOfTask}");
            Console.WriteLine($"Дата создания задачи : {_EnterDateOfTask}");
            Console.WriteLine($"DeadLine Дата задачи: {_DeadLine}");
        }
        public static Task New()
        {
           
            Console.Write("Введите ID для вашей задачи: ");
            int id = Int32.Parse(Console.ReadLine());
            Console.Write("Введите название задачи: ");
            string Name = Console.ReadLine();
            Console.Write("Введите положение задачи: \n Сделано , В прогрессе, Только введен\n");
            string Status = Console.ReadLine();
            Console.Write("Введите приоритет задачи(1-4): ");
            int Priority = Int32.Parse(Console.ReadLine());
            Console.Write("Введите дату ввода задачи: ");
            DateTime date = DateTime.Parse(Console.ReadLine());
        Again:
            Console.Write("Введите примерную дату DeadLine: ");
 
            DateTime dead = DateTime.Parse(Console.ReadLine());
            if (date > dead)
            {
               Console.WriteLine("Дедлайн не может быть раньше объявления начала задачи. Попробуйте еще раз\n");
                goto Again;
            }
            return new Task(id, Name, Status, Priority, date, dead);
        }
        public class DataLoader

        {

            const string directory = "../../Data";

            const string fileName = "jsontasks.json";


            public static void Save(Task[] companies)

            {

                string json = JsonConvert.SerializeObject(companies);

                SaveFile(json);

            }


            public static Task[] Load()

            {

                try

                {

                    string content = File.ReadAllText($"{directory}/{fileName}");

                    if (string.IsNullOrEmpty(content))

                    {

                        throw new ApplicationException($"файл {fileName} пустой");

                    }

                    Task[] tasks = JsonConvert.DeserializeObject<Task[]>(content);


                    return tasks;

                }

                catch (DirectoryNotFoundException)

                {

                    Console.WriteLine($"Отсутствует директория {directory}");

                    return new Task[0];

                }

                catch (FileNotFoundException)

                {

                    Console.WriteLine($"Отсутствует файл {fileName}");

                    return new Task[0];

                }

                catch (ApplicationException ex)

                {

                    Console.WriteLine(ex.Message);

                    return new Task[0];

                }

            }


            private static void SaveFile(string content)

            {

                try

                {

                    File.WriteAllText($"{directory}/{fileName}", content);

                }

                catch (DirectoryNotFoundException)

                {

                    Directory.CreateDirectory(directory);

                    SaveFile(content);

                }

            }

        }


        class Program
        {
            static void Main(string[] args)
            {





                DateTime date1 = new DateTime(2015, 7, 20);
                DateTime date2 = new DateTime(2017, 4, 28);
                DateTime date3 = new DateTime(2011, 1, 12);
                DateTime date4 = new DateTime(2013, 2, 26);
                DateTime dead1 = new DateTime(2016, 4, 28);
                DateTime dead2 = new DateTime(2018, 2, 25);
                DateTime dead3 = new DateTime(2012, 3, 11);
                DateTime dead4 = new DateTime(2013, 4, 14);

                List<Task> tasks = new List<Task>()
            {


            new Task(){_idOfTask= 11,
                _NameOfTask = "Решить пример из учебника алгербы",
                _StatusOfTask = "Только введен",
                _PriorityOfTask = 1,
                _EnterDateOfTask = date1 ,
                _DeadLine = dead1 },
                  
                    new Task(){_idOfTask= 22,
                        _NameOfTask = "Начертить ровный треугольник без линейки",
                        _StatusOfTask = "В прогрессе",
                        _PriorityOfTask = 3,
                        _EnterDateOfTask = date2 ,
                        _DeadLine = dead2},
                    
                    new Task(){_idOfTask= 33,
                        _NameOfTask = "Повесить картину на стене",
                        _StatusOfTask = "Сделано",
                        _PriorityOfTask = 3,
                        _EnterDateOfTask = date3,
                        _DeadLine = dead3 },
                     
                    new Task(){_idOfTask= 44,
                        _NameOfTask = "Написать руководству компании",
                        _StatusOfTask = "В прогрессе",
                        _PriorityOfTask = 1,
                        _EnterDateOfTask = date4 ,
                        _DeadLine = dead4},
            };

                int prior = 1;
                foreach (Task task in tasks.Where(cr => cr._PriorityOfTask >= prior))
                    task.PrintTask();


               


                int choice;
                string again;
                do
                {
                    Console.WriteLine();
                    Console.WriteLine("1.Добавление новой задачи");
                    Console.WriteLine("2.Изменение статуса задачи или ее удаление");
                    Console.WriteLine("3.Выбор временного dead-лайна для задачи");
                    Console.WriteLine("4.Выбор действия при наступлении dead - лайна");
                    Console.WriteLine("5.Сортировка списка задач(по умолчанию список задач отсортирован по дате добавления задачи в порядке от самой новой)");
                    choice = Int32.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:

                            tasks.Add(Task.New());
                            break;
                        case 2:
                            
                            Console.WriteLine("Что вы именно хотите сделать?\n 1 - Удалить , 2 - Изменить");
                            int x;
                            x = Int32.Parse(Console.ReadLine());
                            if (x == 1)
                            {
                                Delete:
                                Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы удалить : ");
                                int del = Convert.ToInt32(Console.ReadLine());
                                var ToRemove = tasks.Find(r => r._idOfTask == del);
                                  if (ToRemove._StatusOfTask == "В прогрессе")
                                {
                                    Console.WriteLine("Вы не можете удалить эту задачу. Попробуйте другую задачу.");
                                    goto Delete;
                                }
                                    if (ToRemove != null)
                                    tasks.Remove(ToRemove);
                                Console.WriteLine($"{ToRemove._idOfTask} , {ToRemove._NameOfTask} , {ToRemove._StatusOfTask} , {ToRemove._PriorityOfTask} , { ToRemove._EnterDateOfTask}");
                                Console.WriteLine("------------Успешно удалено---------------");
                            }
                            else if (x == 2)
                            {
                                Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы изменить его статус : ");
                                int change = Convert.ToInt32(Console.ReadLine());
                                var taskToChange = tasks.Find(c => c._idOfTask == change);

                                if (taskToChange != null)
                                    tasks.IndexOf(taskToChange);
                                Console.WriteLine($"Нынешний статус задачи : {taskToChange._StatusOfTask}\n");
                            Make:
                                Console.WriteLine($"Введите новый статус для задачи <<{taskToChange._NameOfTask}>>\n Только введен, В прогрессе , Сделано");
                                string Status = Console.ReadLine();

                                if (taskToChange._StatusOfTask == "Только введен")
                                {
                                    if (Status == "Сделано")
  {
                                        Console.WriteLine("Вы не можете поставить такой статус задаче. Попробуйте еще раз.");
                                        goto Make;
                                    }
                              
                                   taskToChange._StatusOfTask = Status;
                                



                            }
                                else if (taskToChange._StatusOfTask == "В прогрессе")
                                {
                                    if (Status == "В прогрессе")
                                    {
                                        Console.WriteLine("Вы не можете поставить такой статус задаче. Попробуйте еще раз.");
                                        goto Make;
                                    }

                                    taskToChange._StatusOfTask = Status;
                                }
                                else if (taskToChange._StatusOfTask == "Сделано")
                                {
                                    if (Status == "Сделано")
                                    {
                                        if (Status == "В прогрессе")
                                        {
                                            Console.WriteLine("Вы не можете поставить такой статус задаче. Попробуйте еще раз.");
                                            goto Make;
                                        }
                                    }
                                    taskToChange._StatusOfTask = Status;





                                }
                            }

                            break;
                        case 3:
                            Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы поставить DeadLine : ");
                            int set = Convert.ToInt32(Console.ReadLine());
                            var ToSet = tasks.Find(r => r._idOfTask == set);
                            Console.WriteLine($"Дата создания этой задачи: {ToSet._EnterDateOfTask}");
                            Tagain:
                            Console.Write($"Введите время ДедЛайна вашей задачи <<{ToSet._NameOfTask}>>\n");
                            DateTime datedead = DateTime.Parse(Console.ReadLine());
                            if (ToSet._EnterDateOfTask > datedead)
                            {
                                Console.WriteLine("Дедлайн не может быть раньше объявления начала задачи. Попробуйте еще раз\n");
                                goto Tagain;
                             
                            }

                            
                            break;

                        case 4:
                            Console.WriteLine("Введите ID (Идентификатор) вашей задачи, чтобы поставить ему действие при наступлении DeadLine : ");
                            int come = Convert.ToInt32(Console.ReadLine());
                            var ToCome = tasks.Find(r => r._idOfTask == come);
                            Console.WriteLine($"Задача <<{ToCome._NameOfTask}>> с Дедлайном в {ToCome._DeadLine}");
                            Console.WriteLine("Какое действие вы хотите сделать с этой задачей?\n 1 - Удалить при окончании DeadLine , 2 - Поставить пометку");
                            int chice;
                           
                            chice = Int32.Parse(Console.ReadLine());
                            if (chice == 1)
                            {
                             
                                    Console.WriteLine("Вы уверены, что хотите удалить эту задачу по окончании DeadLine? (y/n)");
                                string answer = Console.ReadLine();
                                    if (answer == "y")
                                {
                                    Console.WriteLine($"Данные сохранены. Задача <<{ToCome._NameOfTask}>> будет удалена по окончанию DeadLine.");
                                } 
                               
                                
                            }
                            if (chice == 2)
                            {
                                Console.WriteLine("Оставьте любой символ, чтобы как-то отметить задачу");
                                string symbol = Console.ReadLine();
                                Console.WriteLine($"Будет выглядеть так --> {ToCome._NameOfTask} {symbol}");
                               
                            }
                            break;
                        case 5:
                            Console.WriteLine();
                            Console.WriteLine("Применение метода сортировки");
                            Console.WriteLine();
                            Console.WriteLine("Сортировка по НАЗВАНИЮ задачи");
                            Console.WriteLine();
                            var SortTask = from t in tasks
                                           orderby t._NameOfTask
                                           select t;

                            foreach (Task t in SortTask)
                                Console.WriteLine(t._NameOfTask);


                            Console.WriteLine();
                            Console.WriteLine("Сортировка по СТАТУСУ задачи");
                            Console.WriteLine();
                            var SortTaskStat = from s in tasks
                                               orderby s._StatusOfTask
                                               select s;

                            foreach (Task s in SortTaskStat)
                                Console.WriteLine(s._StatusOfTask);

                            Console.WriteLine();
                            Console.WriteLine("Сортировка по ПРИОРИТЕТУ задачи");
                            Console.WriteLine();
                            var SortTaskPrior = from task in tasks
                                                orderby task._PriorityOfTask
                                                select task;
                            foreach (Task task in SortTaskPrior)
                                Console.WriteLine($"{task._PriorityOfTask}");

                            Console.WriteLine();
                            Console.WriteLine("Сортировка по ДАТЕ СОЗДАНИЯ задачи");
                            Console.WriteLine();
                            var SortTaskDate = from tusk in tasks
                                               orderby tusk._EnterDateOfTask
                                               select tusk;
                            foreach (Task tusk in SortTaskDate)
                                Console.WriteLine(tusk._EnterDateOfTask);

                            Console.WriteLine();
                            Console.WriteLine("Сортировка по ДАТЕ DEADLINE задачи");
                            Console.WriteLine();
                            var SortDeadLine = from dead in tasks
                                               orderby dead._DeadLine
                                               select dead;
                            foreach (Task dead in SortDeadLine)
                                Console.WriteLine(dead._DeadLine);
                            break;

                    } 
                        Console.WriteLine();
                    Console.WriteLine("--Готово!--");
                    Console.WriteLine("Продолжить (y/n)? ");

                    again = Console.ReadLine();





                } while (again == "y");


                string jsonData = JsonConvert.SerializeObject(tasks);
            }

        }
    }
}

