﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Zadanie
{
    public class Task
    {
        public string _NameOfTask;
        public string _StatusOfTask;
        public int _PriorityOfTask;
        public DateTime _EnterDateOfTask;

        public static void Sort()
        {
            Sort();
        }


         public void PrintTask()
           {

                   Console.WriteLine("--------------");
               Console.WriteLine($"Название задачи : {_NameOfTask}");
               Console.WriteLine($"Статус задачи : {_StatusOfTask}");
               Console.WriteLine($"Приоритет задачи : {_PriorityOfTask}");
               Console.WriteLine($"Дата создания задачи : {_EnterDateOfTask}");
           }
       } 

        class Program
        {
            static void Main(string[] args)
            {
                DateTime date1 = new DateTime(2015, 7, 20);
                DateTime date2 = new DateTime(2017, 4, 28);
                DateTime date3 = new DateTime(2011, 1, 12);
                DateTime date4 = new DateTime(2013, 2, 26);
                List<Task> tasks = new List<Task>()
            {


            new Task(){ _NameOfTask = "Решить пример из учебника алгербы", _StatusOfTask = "Только введен в список задач", _PriorityOfTask = 1, _EnterDateOfTask = date1 },
                  new Task(){ _NameOfTask = "Начертить ровный треугольник без линейки", _StatusOfTask = "В прогрессе", _PriorityOfTask = 2, _EnterDateOfTask = date2 },
                    new Task(){ _NameOfTask = "Повесить картину на стене", _StatusOfTask = "Сделано", _PriorityOfTask = 3, _EnterDateOfTask = date3 },
                     new Task(){ _NameOfTask = "Написать руководству компании", _StatusOfTask = "В прогрессе", _PriorityOfTask = 4, _EnterDateOfTask = date4 },
            };

            int prior = 1; 
            foreach (Task task in tasks.Where(cr => cr._PriorityOfTask >= prior))
                task.PrintTask();


            Console.WriteLine();
            Console.WriteLine("Применение метода сортировки");
            Console.WriteLine();
            Console.WriteLine("Сортировка по НАЗВАНИЮ задачи");
            Console.WriteLine();
            var SortTask = from t in tasks
                              orderby t._NameOfTask
                              select t;

            foreach (Task t in SortTask)
                Console.WriteLine(t._NameOfTask);


            Console.WriteLine();
            Console.WriteLine("Сортировка по СТАТУСУ задачи");
            Console.WriteLine();
            var SortTaskStat = from s in tasks
                           orderby s._StatusOfTask
                           select s;

            foreach (Task s in SortTaskStat)
                Console.WriteLine(s._StatusOfTask );

            Console.WriteLine();
            Console.WriteLine("Сортировка по ПРИОРИТЕТУ задачи");
            Console.WriteLine();
            var SortTaskPrior = from task in tasks
                                where task._PriorityOfTask > 0
                                select task;
            foreach (Task task in SortTaskPrior)
                Console.WriteLine($"{task._PriorityOfTask}");

            Console.WriteLine();
            Console.WriteLine("Сортировка по ДАТЕ СОЗДАНИЯ задачи");
            Console.WriteLine();
            var SortTaskDate = from tusk in tasks
                               orderby tusk._EnterDateOfTask
                               select tusk;
            foreach (Task tusk in SortTaskDate)
                Console.WriteLine(tusk._EnterDateOfTask);
            Console.ReadLine();

        }

    }
        }
    


  