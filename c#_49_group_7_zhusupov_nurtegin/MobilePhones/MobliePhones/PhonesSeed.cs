﻿

using MobilePhones.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobilePhones
{
    public class PhonesSeed
    {
        public static void CreateData(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<MobileContext>();

            if (!context.Phones.Any())
            {
                context.Phones.Add(entity: new Phone() { Name = "IPhone 6", Company = "Apple", Country = "USA", Price = 400 });
                context.Phones.Add(entity: new Phone() { Name = "Redmi 2", Company = "Xiaomi", Country = "China", Price = 1000 });
                context.Phones.Add(entity: new Phone() { Name = "Samsung galaxy s7", Company = "Samsung", Country = "Korea", Price = 1000 });
                context.Phones.Add(entity: new Phone() { Name = "Huawei Mate4", Company = "Huawei", Country = "China", Price = 1000 });

                context.SaveChanges();
            }
        }
    }
}
