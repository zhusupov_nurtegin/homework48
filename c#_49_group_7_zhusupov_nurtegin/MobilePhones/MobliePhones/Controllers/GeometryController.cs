﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MobilePhones.Models;

namespace MobilePhones.Controllers
{
    public class GeometryController : Controller
    {
        private readonly IHostingEnvironment appEnvironment;
        public GeometryController(IHostingEnvironment appEnvironment)

        {

            this.appEnvironment = appEnvironment;

        }
        public IActionResult DownloadFile()

        {

            string filePath = Path.Combine(appEnvironment.ContentRootPath, "MyFiles/Images.pdf");

            string fileType = "application/pdf";

            string fileName = "Images.pdf";

            return PhysicalFile(filePath, fileType, fileName);

        }
        public FileResult GetFileBytes()

        {

            string path = Path.Combine(appEnvironment.ContentRootPath, "MyFiles/Images2.pdf");

            byte[] array = System.IO.File.ReadAllBytes(path);

            string fileType = "application/pdf";

            string fileName = "Images2.pdf";

            return File(array, fileType, fileName);

        }
        public FileResult GetFileStream()

        {

            string path = Path.Combine(appEnvironment.ContentRootPath, "MyFiles/Images3.pdf");

            FileStream stream = new FileStream(path, FileMode.Open);

            string fileType = "application/pdf";

            string fileName = "Images3.pdf";

            return File(stream, fileType, fileName);

        }
        public VirtualFileResult GetVirtualFile()

        {

            var filepath = Path.Combine("~/MyFiles", "Images.pdf");

            return File(filepath, "application/pdf", "Images.pdf");

        }






        public IActionResult Index(int id, int quantity, string str)
        {
            ViewBag.Id = id;
            ViewBag.Quantity = quantity;
            ViewBag.Stroka = str;
       
            return View();
        }
        [HttpPut]
        public string Square(int width, int height,string str)

        {

            ///return $" - HttpPut - Площадь прямоугольника с основанием {rectangle.Width } и высотой {rectangle.Height} равна {rectangle.CalculateSquare()}";
            double square = width * height;

            return $" -  Площадь прямоугольника с основанием {width} и высотой {height} равна {square}";
        }
        /*public IActionResult Square()

        {

            return View();

        }*/
        [HttpPost, HttpPut]
        public string Square(int width, int height)

        {

            double square = width * height;
           
           
             return $" - HttpPost - Площадь прямоугольника с основанием {width} и высотой {height} равна {square}";
            
           

        }

        public string Sum(int[] numbers)

        {

            return "Сумма чисел равна" + numbers.Sum();

        }
        public IActionResult Squares(Rectangle[] rectangles)
        {
            return View(rectangles.ToList());
        }



    }
    
}
