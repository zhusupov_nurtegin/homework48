﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MobilePhones.Migrations
{
    public partial class AddAddressToUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.AddColumn<string>(
        name: "Address",
        table: "Users",
        nullable: true);

        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Users");
        }
    }
}
