﻿ using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Homework45
{
    class Program
    {
        //public string[] Split(params char[]? separator);
        public string UserAgent { get; }

        static void Main(string[] args)
        {
            Listen();
            Console.ReadLine();
            static async Task Listen()
            {
                HttpListener listener = new HttpListener();
                listener.Prefixes.Add("http://localhost:7777/mysite/");
                listener.Start();
                Console.WriteLine("Ждем подключений");
                int counter = 1;
            
                while (true)
                {
                 
                    HttpListenerContext context = await listener.GetContextAsync();
                    HttpListenerRequest request = context.Request;
                    NameValueCollection headers = request.Headers;
                   
                    Console.WriteLine($"Заголовки запроса - № {counter} ");
                    foreach (string key in headers.AllKeys)
                    {
                        string[] values = headers.GetValues(key);
                        if (values.Length > 0)
                        {
                            Console.WriteLine("Значение заголовка {0} : ", key);
                            foreach (string value in values)
                            {
                                Console.WriteLine("   {0}", value);
                            }
                        }
                        else
                        {
                            Console.WriteLine("There is no value associated with the header.");
                        }
                    }
            
                  
                    HttpListenerResponse response = context.Response;
                    
                    NameValueCollection myHeaders = new NameValueCollection();
                    string userAgent;
                    userAgent = request.UserAgent;
                    if (userAgent.IndexOf("Chrome")> 1)
                    {
                        myHeaders.Add("Requested-Chrome", "true");
                    }
                    if (userAgent.IndexOf("Edge") > 1)
                    {
                        myHeaders.Add("Requested-Edge", "true");
                    }
                   
                    response.Headers.Add(myHeaders);
                   
                


                string responseStr = $"<html><head><meta charset='utf8'> </head><body><p>Hello World! Ваш номер запроса: {counter}</p> <p class='lebov'>\n{request.Headers}\n </p> </body></html>";        
                    byte[] buffer = Encoding.UTF8.GetBytes(responseStr);
                    response.ContentLength64 = buffer.Length;
                    Stream output = response.OutputStream;
                    output.Write(buffer, 0, buffer.Length);
              
            output.Close();
                    Console.WriteLine($"Конец запроса № {counter}");
                    counter += 1;
                    
                
                }

               
            }
          
        }
    }
}
