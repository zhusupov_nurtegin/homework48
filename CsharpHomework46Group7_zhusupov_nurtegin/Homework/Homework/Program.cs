﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Test 
{
    class Program /* Я также добавил метод Evalueate(см.ниже). Пример ввода ?expression=(32-1)/4. При неправильном
        вводе сработает catch*/
        
    {
        public string UserAgent { get; }
        static void  Main(string[] _)
        {
            System.Web.HttpUtility.UrlEncode("@");
            Listen();
            Console.ReadLine();
            static async Task Listen()
            {
                HttpListener listener = new HttpListener();
                listener.Prefixes.Add("http://localhost:7777/mysite/");
                listener.Start();
                Console.WriteLine("Ждем подключений");
                int counter = 1;

                while (true)
                {

                    HttpListenerContext context = await listener.GetContextAsync();
                    HttpListenerRequest request = context.Request;
                    NameValueCollection headers = request.Headers;

                    NameValueCollection requestParameters = request.QueryString;

                    Console.WriteLine($"Заголовки запроса - № {counter} ");
                    foreach (string key in headers.AllKeys)
                    {
                        string[] values = headers.GetValues(key);
                        if (values.Length > 0)
                        {
                            Console.WriteLine("Значение заголовка {0} : ", key);
                            foreach (string value in values)
                            {
                                Console.WriteLine("   {0}", value);
                            }
                        }
                        else
                        {
                            Console.WriteLine("There is no value associated with the header.");
                        }
                    }

                    HttpListenerResponse response = context.Response;

                    NameValueCollection myHeaders = new NameValueCollection();
                    string userAgent;
                    userAgent = request.UserAgent;
                    if (userAgent.IndexOf("Chrome") > 1)
                    {
                        myHeaders.Add("Requested-Chrome", "true");
                    }
                    if (userAgent.IndexOf("Edge") > 1)
                    {
                        myHeaders.Add("Requested-Edge", "true");
                    }

                    response.Headers.Add(myHeaders);

                    string responseStr = $"<html><head><meta charset='utf8'> </head><body>Ваш номер запроса: { counter}";
                    responseStr += $"<h1>Параметры</h1>";
                    foreach (string key in requestParameters)
                    {
                        responseStr += $"Параметр : {key} - ";
                        foreach (string value in requestParameters.GetValues(key))
                        {
                            if (key == "expression")
                            {
                                try
                                {
                                    responseStr += $"| : {value} = {Evaluate(value)} <br> ";
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            else
                                responseStr += $"| : {value} <br> ";
                        }
                    }
                

                    responseStr += $" </body></html>";
                    byte[] buffer = Encoding.UTF8.GetBytes(responseStr);
                    response.ContentLength64 = buffer.Length;
                    Stream output = response.OutputStream;
                    output.Write(buffer, 0, buffer.Length);

                    output.Close();
                    Console.WriteLine($"Конец запроса № {counter}");
                    counter += 1;
                }
            }
        }

        // Вот этот метод нашел в доках, когда делал 43-44 дз. 
        public static double Evaluate(string expression)
        {
            System.Data.DataTable table = new System.Data.DataTable();
            table.Columns.Add("expression", string.Empty.GetType(), expression);
            System.Data.DataRow row = table.NewRow();
            table.Rows.Add(row);
            return double.Parse((string)row["expression"]);
        }
    }
}