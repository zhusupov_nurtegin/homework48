﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace UpdChjat
{
    class Program

    {

        private static IPAddress remoteIPAddress;

        private static int remotePort;

        private static int localPort;
       

        [STAThread]

        static void Main(string[] args)

        {

            try

            {

                localPort = new Random().Next(49152, 65535);
                Console.WriteLine("Ваш локальный порт:" );
                Console.WriteLine(localPort);
              

                Console.WriteLine("Укажите удаленный порт: ");

                remotePort = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Укажите удаленный IP-адрес: ");

                remoteIPAddress = IPAddress.Parse(Console.ReadLine());

                Thread recieverThread = new Thread(Receiver);

                recieverThread.Start();



                Sender();



            }

            catch (Exception ex)

            {

                Console.WriteLine("Возникло исключение: " + ex.ToString() + "\n  " + ex.Message);

            }

        }


        private static void Sender()

        {

            while (true)

            {

                string datagram = Console.ReadLine();

                UdpClient sender = new UdpClient();

                IPEndPoint endPoint = new IPEndPoint(remoteIPAddress, remotePort);


                try

                {



                    byte[] bytes = Encoding.UTF8.GetBytes(datagram);

                    sender.Send(bytes, bytes.Length, endPoint);

                }

                catch (Exception ex)

                {

                    Console.WriteLine("Возникло исключение: " + ex.ToString() + "\n  " + ex.Message);

                }

                finally

                {

                    sender.Close();

                }

            }

        }


        public static void Receiver()

        {

     

            UdpClient receivingUdpClient = new UdpClient(localPort);
           

            IPEndPoint RemoteIpEndPoint = null;



            try

            {

                while (true)

                {
                    string datetimesms = DateTime.Now.ToString("HH:mm");
                    byte[] receiveBytes = receivingUdpClient.Receive(ref RemoteIpEndPoint);


                    string returnData = Encoding.UTF8.GetString(receiveBytes);

                    Console.WriteLine(" >>> " + returnData + "|" + datetimesms + "|", "((Порт:" + remotePort + "))");

                    
                    
                }

            }

            catch (Exception ex)

            {

                Console.WriteLine("Возникло исключение: " + ex.ToString() + "\n  " + ex.Message);

            }

        }

    }


}
