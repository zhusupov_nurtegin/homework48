﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Task_1
{
    public class MyNode
    {
        private object _Data;
        private MyNode _Next;
        private MyNode _Prev;
        public object Value
        {
            get { return _Data; }
            set { _Data = value; }
        }
        public MyNode(object Data)
        {
            this._Data = Data;
        }
        public MyNode Next
        {
            get { return this._Next; }
            set { this._Next = value; }
        }
        public MyNode Prev
        {
            get { return this._Prev; }
            set { this._Prev = value; }
        }
    }


    class Linked_Lists
    {
        private MyNode First;
        private MyNode Current;
        private MyNode Last;
        private uint size;

        public Linked_Lists()
        {
            size = 0;
            First = Current = Last = null;
        }

        public bool isEmpty
        {
            get
            {
                return size == 0;
            }
        }

        public void Add(object new_Data)
        {
            MyNode node = new MyNode(new_Data);
            // если список пуст
            if (First == null)
            {
                First = node;
                Last = node;
                Last.Next = First;
            }
            else
            {
                node.Next = First;
                Last.Next = node;
                Last = node;
            }
        }




        public void Insert_Index(object newElement, uint index)
        {
            if (index < 1 || index > size)
            {
                throw new InvalidOperationException();
            }
            else if (index == 1)
            {
                Push_Front(newElement);
            }
            else if (index == size)
            {
                Push_Back(newElement);
            }
            else
            {
                uint count = 1;
                Current = First;
                while (Current != null && count != index)
                {
                    Current = Current.Next;
                    count++;
                }
                MyNode newNode = new MyNode(newElement);
                Current.Prev.Next = newNode;
                newNode.Prev = Current.Prev;
                Current.Prev = newNode;
                newNode.Next = Current;
            }
        }

        public void Push_Front(object newElement)
        {
            MyNode newNode = new MyNode(newElement);

            if (First == null)
            {
                First = Last = newNode;
            }
            else
            {
                newNode.Next = First;
                First = newNode;
                newNode.Next.Prev = First;
            }
        }

        public void Push_Back(object newElement)
        {
            MyNode newNode = new MyNode(newElement);

            if (First == null)
            {
                First = Last = newNode;
            }
            else
            {
                Last.Next = newNode;
                newNode.Prev = Last;
                Last = newNode;
            }

        }
        public MyNode Pop_Back()
        {
            if (Last == null)
            {
                throw new InvalidOperationException();
            }
            else
            {
                MyNode temp = Last;
                if (Last.Prev != null)
                {
                    Last.Prev.Next = null;
                }
                Last = Last.Prev;

                return temp;
            }

        }
        class Program
        {
            static void Main()
            {
                Linked_Lists circularList = new Linked_Lists();

                circularList.Add("Tom");
                circularList.Add("Bob");
                circularList.Add("Alice");
                circularList.Add("Jack");





            }
        }
    }
}

 