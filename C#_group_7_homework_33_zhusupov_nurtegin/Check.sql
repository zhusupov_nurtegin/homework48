create table Slayes (Id int, date_of_incoming datetime , category_of_item nchar(15), name_of_item nchar(15),
brand_of_item nchar(15), price decimal, amount int)

select * from Slayes

insert into Slayes values (1, 2019-03-11, 'Toys' , 'Pony', 'XStar', 800, 30)
insert into Slayes values (2, 2020-03-11, 'Machine' , 'Computer', 'Acer', 13000, 6)
insert into Slayes values (3, 2019-03-11, 'Health' , 'OxygenMaker', 'HelpToday', 100000, 3)
insert into Slayes values (4, 2018-03-11, 'Sport' , 'Baseball', 'HeadCare', 300, 80)
insert into Slayes values (5, 2019-03-11, 'School' , 'BackPack', 'LeBruise', 1600, 10)
insert into Slayes values (6, 2020-03-11, 'Technique' , 'Tost', 'Beko', 1900, 9)
insert into Slayes values (7, 2018-03-11, 'Construction' , 'Hammer', 'Steal', 200, 30)
insert into Slayes values (8, 2017-03-11, 'Cosmetics' , 'Pomade', 'KsKIN', 1400, 20)
insert into Slayes values (9, 2020-03-11, 'Games' , 'Fifa', 'EA SPORTS', 800, 100)
SELECT * FROM Slayes ORDER BY date_of_incoming

